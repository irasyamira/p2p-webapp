#!/usr/bin/python
""" MainApp.py

    COMPSYS302 - Software Design
    Author: Ira Syamira (isuk218@aucklanduni.ac.nz)

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)

import urllib2
import hashlib
import webbrowser
import socket
import cherrypy
import sqlite3
import time
import calendar
import json
import datetime
import os
import base64

def getIPAddress():
    try:
        IPAddress = socket.gethostbyname(socket.getfqdn())
    except:
        IPAddress = socket.gethostbyname(socket.gethostname())
    return IPAddress

# The address we listen for connections on
listen_ip = getIPAddress()
listen_port = 10000
hash_salt = "COMPSYS302-2017"
enc = 0
def_url = 'http://cs302.pythonanywhere.com/'

#   open("staticatabase.db","w+")

staticdatabase = sqlite3.connect('staticdatabase.db', check_same_thread=False)    
database = sqlite3.connect('database.db', check_same_thread=False)
db1 = database.cursor()
db2 = staticdatabase.cursor()
date = time.strftime('%Y-%m-%d', time.gmtime())

def checkLocation():
    try:
        position = listen_ip.index('10.103')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 0
    
    try:
        position = listen_ip.index('10.104')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 0
    
    try:
        position = listen_ip.index('130.216')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 0
        
    try:
        position = listen_ip.index('172.23')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 1
    
    try:
        position = listen_ip.index('172.24')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 1
    
    try:
        position = listen_ip.index('172.18')
    except ValueError:
        position = -1
    
    if (position != -1):
        return 1
    return 2

webbrowser.open_new('http://%s:%d/login' % (listen_ip, listen_port))

class MainApp(object):

    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }                 
    
    #If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "<center>I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #Logs an online user by passing an array of 5 elements containing the user details
    def logOnlineUser(self, userDetails):
        username = userDetails[0] 
        location = userDetails[1]
        ip = userDetails[2]
        port = userDetails[3]
        loginTime = userDetails[4]   
        db1.execute('''INSERT INTO onlineUsers (username, location, ip, port, loginTime)
        VALUES (?,?,?,?,?)''', (username, location, ip, port, loginTime))
        database.commit()

    #Deletes a user with an input parameter of the offline user's username
    def deleteOfflineUser(self, username):
        db1.execute('''DELETE FROM onlineUsers
        WHERE username = ?''', (username,))        
        database.commit()
    
    #Fetches a user's detail based on the input parameters username and the column/detail of interest
    def getOnlineUserDetails(self, username, column):
        db1.execute('''SELECT * FROM onlineUsers
        WHERE username = ?''', (username,))        
        result = db1.fetchone()
        try:
            if (column == '*'):
                return result
            else:
                if (column == 'id'):
                    j = 0
                elif (column == 'username'):
                    j = 1
                elif (column == 'location'):
                    j = 2
                elif (column == 'ip'):
                    j = 3
                elif (column == 'port'):
                    j = 4
                elif (column == 'loginTime'):
                    j = 5
                return result[j]
        except:
            print 'There is an error in fetching the user''s details'
            raise cherrypy.HTTPRedirect('/inactivity') 

    #Fetches a user's detail based on the input parameters id and the column/detail of interest
    def getOnlineUserBasedonID(self, id, column):
        db1.execute('''SELECT * FROM onlineUsers
        WHERE id = ?''', (id,))        
        result = db1.fetchone()
        if (column == '*'):
            return result
        else:
            if (column == 'id'):
                j = 0
            elif (column == 'username'):
                j = 1
            elif (column == 'location'):
                j = 2
            elif (column == 'ip'):
                j = 3
            elif (column == 'port'):
                j = 4
            elif (column == 'loginTime'):
                j = 5
            return result[j]            
            
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #Creates a sentMessages table in the static database 
    #Static database - (not being reset upon application opening to retain messaging history)
    #Returns a 1 if the table has been succesfully created and 0 otherwise
    #A query cursor is also created for the staticdatabase    
    def createSentMessagesTable():
        db2 = staticdatabase.cursor()
        try:
            db2.execute('''CREATE TABLE IF NOT EXISTS sentMessages
            (id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, time TEXT)''')
        except:
            return 1
        staticdatabase.commit()
        return 0 
    
    #Logs a sentMessage by passing an array of 4 elements containing the sentMessage details
    def logSentMessage(self, messageDetails):
        sender = messageDetails[0] 
        destination = messageDetails[1]
        message = messageDetails[2]
        time = messageDetails[3]
        db2.execute('''INSERT INTO sentMessages (sender, destination, message, time)
        VALUES (?,?,?,?)''', (sender, destination, buffer(message), time))
        staticdatabase.commit() 

    #Fetches a sentMessage detail based on the input parameters id and the column/detail of interest
    def getSentMessage(self, id, column):
        db2.execute('''SELECT * FROM sentMessages
        WHERE id = ?''', (id,))        
        result = db2.fetchone()
        if (column == '*'):
            return result
        else:
            if (column == 'id'):
                j = 0
            elif (column == 'sender'):
                j = 1
            elif (column == 'destination'):
                j = 2
            elif (column == 'message'):
                j = 3                
            elif (column == 'time'):
                j = 4
            return result[j]            
 
    #Creates a sentMessages table and checks if it has been succesfully created
    sentMessagesTableCheck = createSentMessagesTable()
    if (sentMessagesTableCheck == 1):
        print "sentMessages table ccould not be initialised"
    else:
        print "sentMessages table has been succesfully initialised"
 
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #Creates a receivedMessages table in the static database 
    #Static database - (not being reset upon application opening to retain messaging history)
    #Returns a 1 if the table has been succesfully created and 0 otherwise
    #A query cursor is also created for the staticdatabase    
    def createReceivedMessagesTable():
        db2 = staticdatabase.cursor()
        try:
            db2.execute('''CREATE TABLE IF NOT EXISTS receivedMessages
            (id INTEGER PRIMARY KEY, sender TEXT, destination TEXT, message TEXT, time TEXT, file TEXT, filename TEXT, content_type TEXT)''')
        except:
            return 1
        staticdatabase.commit()
        return 0
    
    #Logs a receivedMessage by passing an array of 4 elements containing the receivedMessage details
    def logReceivedMessage(self, messageDetails):
        sender = messageDetails[0] 
        destination = messageDetails[1]
        message = messageDetails[2]
        time = messageDetails[3]
        file = messageDetails[4]
        filename = messageDetails[5]
        content_type = messageDetails[6]
        db2.execute('''INSERT INTO receivedMessages (sender, destination, message, time,file, filename, content_type)
        VALUES (?,?,?,?,?,?,?)''', (sender, destination, message, time, file, filename, content_type))
        staticdatabase.commit() 

    #Fetches a receivedMessage detail based on the input parameters id and the column/detail of interest
    def getReceivedMessage(self, id, column):
        db2.execute('''SELECT * FROM receivedMessages
        WHERE id = ?''', (id,))       
        result = db2.fetchone()
        if (column == '*'):
            return result
        else:
            if (column == 'id'):
                j = 0
            elif (column == 'sender'):
                j = 1
            elif (column == 'destination'):
                j = 2
            elif (column == 'message'):
                j = 3                
            elif (column == 'time'):
                j = 4
            elif (column == 'file'):
                j = 5
            elif (column == 'filename'):
                j = 6
            elif (column == 'content_type'):
                j = 7
            return result[j]            
 
    #Creates a receivedMessages table and checks if it has been succesfully created
    receivedMessagesTableCheck = createReceivedMessagesTable()
    if (receivedMessagesTableCheck == 1):
        print "receivedMessages table ccould not be initialised"        
    else:
        print "receivedMessages table has been succesfully initialised"

 
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #Creates a userDetails table in the static database
    #This table stores information on user profile details through getProfile()/logUserDetails()
    #Static database - (not being reset upon application opening to retain messaging history)
    #Returns a 1 if the table has been succesfully created and 0 otherwise
    #A query cursor is also created for the staticdatabase    
    def createUserProfileTable():
        db2 = staticdatabase.cursor()
        try:
            db2.execute('''CREATE TABLE IF NOT EXISTS userDetails
            (id INTEGER PRIMARY KEY, username TEXT, fullname TEXT, position TEXT, description TEXT, location TEXT, picture TEXT)''')
        except:
            return 1
        staticdatabase.commit()
        return 0
 
    #Deletes a userProfile with an input parameter of username
    def deleteUserProfile(self, username):
        db2.execute('''DELETE FROM userDetails
        WHERE username = ?''', (username,))  
        staticdatabase.commit()
 
    #Checks if a user already has a profile logged into the table in the database based on input parameter username
    #Returns a 1 if the user has been logged into the table in the database and 0 otherwise
    def checkDuplicateProfile(self, username):
        db2.execute('''SELECT * FROM userDetails
        WHERE username = ?''', (username,))
        result = db2.fetchone()
        if (result == None):
            return 0
        else:
            return 1   
 
    #Logs a userProfile by passing in 4 input parameters
    #Redirects user to their profile if their details have been succesfully logged into the table
    #If user has an existing profile logged into the table, it will be deleted and updated details will be logged in
    #without overwriting any empty fields (if the user wishes to not provide the information or wants to keep the old information)
    @cherrypy.expose
    def logUserProfile(self,username=None,fullname=None,position=None,description=None,location=None,picture=None): 
        if username == None:
            username = cherrypy.session['username']
        try: 
            try:
                if self.checkDuplicateProfile(username) == 1:
                    self.deleteUserProfile(username)
                    db2.execute('''INSERT INTO userDetails (username, fullname, position, description, location, picture)
                    VALUES (?,?,?,?,?,?)''', (username,fullname,position,description,location,picture))
                    staticdatabase.commit()
                else:
                    db2.execute('''INSERT INTO userDetails (username, fullname, position, description, location, picture)
                    VALUES (?,?,?,?,?,?)''', (username,fullname,position,description,location,picture))
                    staticdatabase.commit()
            except:
                db2.execute('''INSERT INTO userDetails (username, fullname, position, description, location, picture)
                VALUES (?,?,?,?,?,?)''', (username,fullname,position,description,location,picture))
                staticdatabase.commit()
            #raise cherrypy.HTTPRedirect("/userProfile")    
        except:
            pass
        return 1

    #Fetches a user's profile detail based on the input parameters username and the column/detail of interest
    def getProfileDetails(self, username, column):
        db2.execute('''SELECT * FROM userDetails
        WHERE username = ?''', (username,))        
        result = db2.fetchone()
        if (column == '*'):
            return result
        else:
            if (column == 'id'):
                j = 0
            elif (column == 'username'):
                j = 1
            elif (column == 'fullname'):
                j = 2
            elif (column == 'position'):
                j = 3   
            elif (column == 'description'):
                j = 4
            elif (column == 'location'):
                j = 5
            elif (column == 'picture'):
                j = 6                    
            return result[j]            

    #Fetches a user's profile detail based on the input parameters id and the column/detail of interest
    def getProfileDetailsBasedOnId(self, id, column):
        db2.execute('''SELECT * FROM userDetails
        WHERE id = ?''', (id,))       
        result = db2.fetchone()
        if (column == '*'):
            return result
        else:
            if (column == 'id'):
                j = 0
            elif (column == 'username'):
                j = 1
            elif (column == 'fullname'):
                j = 2
            elif (column == 'position'):
                j = 3   
            elif (column == 'description'):
                j = 4
            elif (column == 'location'):
                j = 5
            elif (column == 'picture'):
                j = 6                    
            return result[j]     
            
    #Creates a userDetails table and checks if it has been succesfully created
    profileDetailsTableCheck = createUserProfileTable()
    if (profileDetailsTableCheck == 1):
        print "userDetails table ccould not be initialised"  
    else:
        print "userDetails table has been succesfully initialised"   

   
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #Renders the HTML setting for the page as well as updating the list of online users
    #each time it is being visited.
    @cherrypy.expose
    def menu(self):
        openHtml = open('bg_menu.html', 'r')
        Page = openHtml.read()
        Page += "<br><br>Today's date: <b>"+ date + '</b><br>'
        return Page
    
    #Renders the HTML setting for the login page which has the login form
    @cherrypy.expose
    def login(self):
        openHtml = open('bg_login.html', 'r')
        Page = openHtml.read()
        return Page  
        
    #Renders the HTML setting for the page on developer info as well as updating the list of online users
    #each time it is being visited.
    @cherrypy.expose
    def aboutTheDeveloper(self):
        self.onlineUsers()
        openHtml = open('bg_developer.html', 'r')
        Page = openHtml.read()
        return Page

    #Searches through the sentMessages and receivedMessages tables which stores messages that are logged into the database
    #Returns the list of messages containing the substring searched, or nothing if not found        
    @cherrypy.expose
    def searchMessage(self,message=None):
        openHtml = open('bg_searchPage.html', 'r')
        Page = openHtml.read()    
        db2.execute('''SELECT max(id) FROM receivedMessages''')
        a = db2.fetchone()
        db2.execute('''SELECT max(id) FROM sentMessages''')
        b = db2.fetchone()        
    
        if str(message) != 'None':
            message = str(message)
            for i in range(a[0]):
                if i != 0:
                    messageCheck = str(self.getReceivedMessage(i,'message'))
                    sender = self.getReceivedMessage(i,'sender')
                    time = self.getReceivedMessage(i,'time')
                    if messageCheck.find(message) != -1:
                        Page += 'received from ' + sender + ': ' + str(messageCheck) + ' at ' + str(time) + '<br>'
                    
            for j in range(b[0]):
                if j != 0:        
                    messageCheck = str(self.getSentMessage(j,'message'))
                    time = self.getSentMessage(j,'time')
                    destination = self.getSentMessage(j,'destination')
                    if messageCheck.find(message) != -1:
                        Page += 'sent to ' + str(destination) + ': ' + str(messageCheck) + ' at ' + str(time) + '<br>'
        return Page                    
        
    #Checks if user has been succesfully logged in
    #Redirects user to /menu if login is successful or returns a login error message
    @cherrypy.expose
    def signin(self, username = None, password = None):
        if (username == None) or (password == None):
            openHtml = open('bg_login.html', 'r')
            Page = openHtml.read()
            Page += '<br>Login was not succesful please try again :('
            return Page
        else:
            error = self.authoriseUserLogin(username,password)
            if (error == "0"):
                messageDetails = ['Message Bot', 'Message Bot', '..Waiting for new messages..', time.time(), None, None, None]
                self.logReceivedMessage(messageDetails)  
                cherrypy.session['username'] = username;
                cherrypy.session['password'] = password;
                raise cherrypy.HTTPRedirect('/menu')
            else:
                openHtml = open('bg_login.html', 'r')
                Page = openHtml.read()
                Page += '<br>Login was not succesful please try again :('
                return Page
    
    #Checks if user has been succesfully logged out
    #Redirects user to /loggedOff if log out is successful or returns a log out error message
    @cherrypy.expose
    def logout(self):
        password = cherrypy.session['password']
        hash_password = hashlib.sha256(str(password)+hash_salt).hexdigest()
        url = def_url + 'logoff?' + 'username=' + str(cherrypy.session['username']) 
        url += '&password=' + hash_password + '&location=' 
        url += str(checkLocation()) + '&ip=' + str(listen_ip) 
        url += '&port=' + str(listen_port) + '&enc=' + str(enc)  
 
        response_message = (urllib2.urlopen(url)).read()
        response = response_message[0]
    
        if (response == "0"):
            raise cherrypy.HTTPRedirect('/loggedOff')
        else:
            return str(response_message) + '<br>Logout was not succesful :('    
    
    #Renders the HTML setting for the 'logged off' message if log off was succesful 
    @cherrypy.expose
    def loggedOff(self):
        openHtml = open('bg_loggedoff.html', 'r')
        Page = openHtml.read()
        Page += "<br>Log off was succesful. Bye bye"
        Page += "<br>Click to <a href='login'>log in</a>"
        return Page
    
    #Renders the HTML setting if a page could not be reached or user has been automatically logged off because
    #of inactivity
    @cherrypy.expose
    def inactivity(self):
        openHtml = open('bg_inactivity.html', 'r')
        Page = openHtml.read()
        Page += "<br>Sorry, you either need to refresh the page or you have been logged out from the server due to inactivity"
        Page += "<br>Click to <a href='login'>log in</a>"
        return Page
   
    #Renders the HTML setting for the user's profile by fetching the user's data using getProfileDetails 
    #May take in an optional username input if the profile displayed needs to be of another user instead of the
    #cureent user logged in to the application (i.e profile search) as well as updating the list of online users
    #each time it is being visited using updateOnlineUsers()
    @cherrypy.expose    
    def userProfile(self,username=None):
        self.onlineUsers()
        openHtml = open('bg_userProfile.html', 'r')
        Page = openHtml.read()
        
        try:
            picture = self.getProfileDetails(cherrypy.session['username'],'picture')
            Page += '<img src="' + picture + '" width = 400/>'
            Page += '<center><br><b>' + self.getProfileDetails(cherrypy.session['username'],'fullname') + "</b>'s profile<br>"
            Page += "<br><sub>Fullname:</sub><br>" + self.getProfileDetails(cherrypy.session['username'],'fullname') 
            Page += "<br><sub>Position:</sub><br>" + self.getProfileDetails(cherrypy.session['username'],'position')
            Page += "<br><sub>Location:</sub><br>" + self.getProfileDetails(cherrypy.session['username'],'location')
            Page += "<br><sub>Description:</sub><br>" + self.getProfileDetails(cherrypy.session['username'],'description')
            Page += "<br><br>Click to <a href='editProfile'>edit</a> your profile" 
        except:
            Page += "<center><br>" + str(cherrypy.session['username'])
            Page += "<br>This is your profile. <br>Please <a href='editProfile'>edit</a> your information and add a profile photo."    
        Page += '<br><br><br><br><img src="http://orig15.deviantart.net/92f9/f/2016/323/d/0/neko_atsume_animation_base__rolling_with_ball_by_keylimejazy-daoyq2t.png"height = 70;/>'
        return Page
    
    #Renders the HTML setting for profile editing page which has a form of 5 fields as well as updating the list of online users
    #each time it is being visited using updateOnlineUsers()    
    @cherrypy.expose
    def editProfile(self):
        self.onlineUsers()
        openHtml = open('bg_editProfile.html', 'r')
        Page = openHtml.read()
        
        return Page
        raise cherrypy.HTTPRedirect("/userProfile")

    #Checks if the credentials given by user is authenticatted and returns a 0 if user has been logged in succesefully, or
    #other numbers according to the human readable error message
    def authoriseUserLogin(self, username, password):
        hash_password = hashlib.sha256(str(password)+hash_salt).hexdigest()
        
        url = def_url + 'report?' + 'username=' + username 
        url += '&password=' + hash_password + '&location=' 
        url += str(checkLocation()) + '&ip=' + str(listen_ip) 
        url += '&port=' + str(listen_port) + '&enc=' + str(enc)
    
        response_message = (urllib2.urlopen(url)).read()
        response = response_message[0]
        return response

    #Renders the HTML settings for the page displaying online users by first updating the list of online users
    #each time it is being called using updateOnlineUsers()
    #Then the max id of the onlineUsers table is being fetched which is used to loop through the username
    #column in the table. As it loops through, the usernames are being returned
    #A boolean is used to check if the current user is still logged in by searching his/her username in the 
    #username column. If it is no longer logged in/online, user will be automatically loggged off
    @cherrypy.expose            
    def onlineUsers(self):
        openHtml = open('bg_onlineUsers.html', 'r')
        Page = openHtml.read() 
        self.updateOnlineUsers()
        db1.execute('''SELECT max(id) FROM onlineUsers''')
        result = db1.fetchone()
        userLoggedIn = 0
        
        for i in range(result[0]):
            if i != 0:
                Page += self.getOnlineUserBasedonID(i,'username') + ' <br>Location: ' + self.getOnlineUserBasedonID(i,'location')  + '<br>'
                if str(self.getOnlineUserBasedonID(i,'username')) == str(cherrypy.session['username']):
                    userLoggedIn = 1
        
        if userLoggedIn == 0:
            raise cherrypy.HTTPRedirect("/inactivity")
        else:
            return Page

    #Retrieves the list of online users from the server and then logs them into the onlineUsers table in the database
    #The table is renewed everytime updateOnlineUsers() is called to ensure no duplicate users are being logged 
    #Splits the list into their respective categories and logs them accordingly in the onlineUsers table
    def updateOnlineUsers(self): 
        url = 'http://cs302.pythonanywhere.com/getList?username=' + str(cherrypy.session['username']) + '&password=' + hashlib.sha256(str(cherrypy.session['password'])+hash_salt).hexdigest() + '&enc=0'
        response = urllib2.urlopen(url)
        error_message = "0, Online user list returned"
        response = response.read().replace(error_message, "")
        response = response.split()

        db1.execute('''DROP TABLE IF EXISTS onlineUsers''')
        database.commit()
        db1.execute('''CREATE TABLE IF NOT EXISTS onlineUsers
        (id INTEGER PRIMARY KEY, username TEXT, location TEXT, ip TEXT, port TEXT, loginTime TEXT)''')
        database.commit()            
                
        for userDetails in response:
            userDetails = userDetails.split(',')
            upi = userDetails[0]
            location = userDetails[1]
            ip = userDetails[2]
            port = userDetails[3]
            loginTime = userDetails[4]
            self.logOnlineUser(userDetails)
            
    #Returns a list of APIs supported by this client
    @cherrypy.expose
    def listAPI(self):
        return '/listAPI<br>/ping[sender]<br>/receiveMessage[sender][destination][message][stamp]<br>/getProfile[profile_username][sender]<br>/receiveFile[sender][destination][file][filename][content_type][stamp]'  

    #Renders the HTML setting for the interact page which allows the user to send a Message or a File
    #by uploading them through an HTML form and then typing in the receiver's username
    #The max id of the receivedMessages table is fetched in order to display the most recent message/file
    #received by the user. Enables user to download the file received by providing a clickable link
    @cherrypy.expose
    def interact(self):
        self.onlineUsers()
        openHtml = open('bg_interact.html', 'r')
        Page = openHtml.read()         
        db1.execute('''SELECT max(id) FROM onlineUsers''')
        a = db1.fetchone()
        Page += '<center>Online users: <br><sub>| '
        for i in range(a[0]):
            if i != 0:
                Page += self.getOnlineUserBasedonID(i,'username') + ' | '             
        db2.execute('''SELECT max(id) FROM receivedMessages''')
        result = db2.fetchone() 
        b = result[0]
        latestMessageFound = 0
        Page += "<center><br></sub>Most recent message(s)/file:<br>"
        
        if self.getReceivedMessage(b,'file') == None: 
            Page += "<br><br><b>" +str(self.getReceivedMessage(b,'message')) + "</b><br>"
            Page += "Sender: " +str(self.getReceivedMessage(b,'sender'))
            Page += "<br>Received on : "+str(self.getReceivedMessage(b,'time')) 
            if self.getReceivedMessage(b-1,'file') == None: 
                Page += "<br><br><b>" +str(self.getReceivedMessage(b-1,'message')) + "</b><br>"
                Page += "Sender: " +str(self.getReceivedMessage(b-1,'sender'))
                Page += "<br>Received on : "+str(self.getReceivedMessage(b-1,'time')) 
                if self.getReceivedMessage(b-2,'file') == None: 
                    Page += "<br><br><b>" +str(self.getReceivedMessage(b-2,'message')) + "</b><br>"
                    Page += "Sender: " +str(self.getReceivedMessage(b-2,'sender'))
                    Page += "<br>Received on : "+str(self.getReceivedMessage(b-2,'time')) 
                else:
                    Page += "<br><br><a href='/download' onclick='myFunction2()'>"+str(self.getReceivedMessage(b-2,'filename'))+"</a><br>"
                    Page += "<br>Sender: " +str(self.getReceivedMessage(b-2,'sender'))
                    Page += "<br>Received on : "+str(self.getReceivedMessage(b-2,'time'))  
            else:
                Page += "<br><br><a href='/download' onclick='myFunction2()'>"+str(self.getReceivedMessage(b-1,'filename'))+"</a><br>"
                Page += "<br>Sender: " +str(self.getReceivedMessage(b-1,'sender'))
                Page += "<br>Received on : "+str(self.getReceivedMessage(b-1,'time'))  
        else:
            Page += "<br><br><a href='/download' onclick='myFunction2()'>"+str(self.getReceivedMessage(b,'filename'))+"</a><br>"
            if str(self.getReceivedMessage(b,'content_type')) == 'mp3':
                Page += '<audio controls="' + 'controls" ><source src="data:audio/mp3;base64,' + self.getReceivedMessage(b,'file') + '"/></audio>'
            elif str(self.getReceivedMessage(b,'content_type')) == 'wav':
                Page += '<audio controls="' + 'controls" ><source src="data:audio/wav;base64,' + self.getReceivedMessage(b,'file') + '"/></audio>'                
            elif str(self.getReceivedMessage(b,'content_type')) == 'jpeg':
                Page += '<img src="data:image/jpeg;base64,' +self.getReceivedMessage(b,'file')+'"height=150; />'
            elif str(self.getReceivedMessage(b,'content_type')) == 'gif':
                Page += '<img src="data:image/gif;base64,' +self.getReceivedMessage(b,'file')+'"height=150; />'                
            elif str(self.getReceivedMessage(b,'content_type')) == 'png':
                Page += '<img src="data:image/png;base64,' +self.getReceivedMessage(b,'file')+'"height=150; />' 
            elif str(self.getReceivedMessage(b,'content_type')) == 'mp4':
                Page += '<video height="240" controls><source type="video/mp4" src="data:video/mp4;base64,' + self.getReceivedMessage(result[0],'file') + '"></video>' 
            else:
                Page += 'File format is not supported, please download it to open the file'
            Page += "<br>Sender: " +str(self.getReceivedMessage(b,'sender'))
            Page += "<br>Received on : "+str(self.getReceivedMessage(b,'time'))    

        openFile = open('bg_interact_2.html', 'r')
        Page += openFile.read()          
        return Page   
 
    #Downloads the file received from another user by writing the stored file (in base64) to
    #a new file in the users' current directory. The file is named as the original filename received.
    #User is redirected back to /interact after download is done
    @cherrypy.expose     
    def download(self):
        db2.execute('''SELECT max(id) FROM receivedMessages''')
        result = db2.fetchone()
        file_data = self.getReceivedMessage(result[0],'file')
        filename = self.getReceivedMessage(result[0],'filename')
        downloadFile = open(filename,'w+')
        downloadFile = open(filename,'wb')          
        downloadFile.write(file_data.decode('base64'))
        downloadFile.close()
        raise cherrypy.HTTPRedirect("/interact")   
 
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////
    #//////////////////////////////////////////////////////////////////////////////////  
    
    #A ping API which returns a 0 if user is still logged into the server
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def ping(self):
        input_data = cherrypy.request.json
        sender = input_data['sender']
        print sender + ' sent you a ping'        
        return '0'
 
    #Pings another user and returns their response
    def pingUser(self,destination):
        sendData = {"sender":cherrypy.session['username']}
        destinationIP = str(self.getOnlineUserDetails(destination,'ip'))
        destinationPort = str(self.getOnlineUserDetails(destination,'port'))           
        data = json.dumps(sendData)
        url = 'http://'+destinationIP+':'+destinationPort+'/ping?'
        req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req).read()
        return response        
    
    #Allows user to receiveMessage from another user by receiving json data which is then processed
    #and logged into the receivedMessages according to their columns.
    #Returns an error code depending on the success of the message receiving process
    #Prints out a message to the console which tells the user that a message has been received along
    #with the sender's username
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        print 'Entered receiveMessage'
        input_data = cherrypy.request.json
        sender = input_data['sender']
        destination = input_data['destination']
        message_utf8 = input_data['message']
        message = message_utf8.encode('utf8')
        stamp = input_data['stamp']
        stamp = time.strftime("%Y-%m-%d %H:%M", time.localtime(stamp)) 
        file = None
        filename = None
        content_type = None
        messageDetails = [sender,destination,buffer(message),stamp, file, filename, content_type]
        self.logReceivedMessage(messageDetails)
        errorMessage = '0: <Action was succesful>'
        print 'Message received from ' + str(sender) + ': ' + str(message)
        return errorMessage    
 
    #Allows user to sendMessageto another user by sending json data which is also being processed
    #and logged into the sentMessages according to their columns.
    #Reads the error code received from the other user
    #Prints out a message to the console which tells the user that a message has been sent along
    #with the receiver's username
    @cherrypy.expose  
    def sendMessage(self, destination = None, message = None):
        message = message.encode('utf8')
        sender = cherrypy.session['username']
        stamp = calendar.timegm(time.gmtime())
        markdown = 0
        file = None
        filename = None
        content_type = None
        messageDetails = [sender,destination,message,time.strftime("%Y-%m-%d %H:%M", time.localtime(stamp)), file, filename, content_type]
        self.logSentMessage(messageDetails)
        destinationIP = str(self.getOnlineUserDetails(destination,'ip'))
        destinationPort = str(self.getOnlineUserDetails(destination,'port'))           
        sendingMessage = {"sender":sender,"destination":destination,"message":message,"stamp":stamp,"markdown": markdown}
        data = json.dumps(sendingMessage)
        url = 'http://'+destinationIP+':'+destinationPort+'/receiveMessage?'
        req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req).read()
        
        if str(response)[0] == '0':
            print 'Message sent to ' + str(destination)
            return '<center><img src="https://s-media-cache-ak0.pinimg.com/originals/96/cb/f1/96cbf1cb56de8d012513832d664d0902.gif" width = 400;/><br><br><body style="background-color:#E7D3CE">Message has been sent to ' + str(destination) + " at " + time.strftime("%Y-%m-%d %H:%M", time.localtime(stamp)) + "<br>Return to <a href='interact'>/interact</a>"                  
  
    #Returns the details on current user on this client and returns it to the sender
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def getProfile(self):
        input_data = cherrypy.request.json
        profile_username = input_data['profile_username']
        sender = input_data['sender']
        print 'getProfile called for ' + str(profile_username) + ' by ' + sender
        try:
            fullname = self.getProfileDetails(profile_username,'fullname')       
            position = self.getProfileDetails(profile_username,'position')
            description = self.getProfileDetails(profile_username,'description')
            location = self.getProfileDetails(profile_username,'location')
            picture = self.getProfileDetails(profile_username,'picture')
            profile = {"fullname":fullname,"position":position,"description":description,"location":location,"picture":picture}
            print 'Profile sent to ' + sender
            return profile
        except:
            print 'getProfile failed to return profile to ' + sender
            pass

    #Fetches a searched user's profile by searching through the userDetails table which stores 
    #profiles. If none is found in the database, getProfile of the user is called 
    #Returns the profile of the user searched, or nothing if not found or not able to retrieve profile            
    @cherrypy.expose
    def requestProfile(self,profile_username): 
        openHtml = open('bg_searchPage.html', 'r')
        Page = openHtml.read()
        openHtml.close()
        #Max id for userDetails is used to loop through all the existing rows in the table
        db2.execute('''SELECT max(id) FROM userDetails''')
        a = db2.fetchone() 
        a = int(a[0])
        i = 1
        userFound = 0
        while i < a + 1:

            try:
                userCheck = self.getProfileDetailsBasedOnId(i, 'username')

                if userCheck == profile_username:
                    userFound = 1
                    Page += '<b>user found!</b><br><br>'
                    picture = self.getProfileDetails(profile_username,'picture')
                    Page += '<img src="' + picture + '" width = 200/>'
                    Page += "<center><br><b>" + profile_username + "</b>'s profile<br>"
                    Page += "<br><sub>Fullname:</sub><br>" + self.getProfileDetails(profile_username,'fullname') 
                    Page += "<br><sub>Location:</sub><br>" + self.getProfileDetails(profile_username,'location')
                    Page += "<br><sub>Description:</sub><br>" + self.getProfileDetails(profile_username,'description')
                    Page += "<br><sub>Position:</sub><br>" + self.getProfileDetails(profile_username,'position')
            except:
                pass
            i = i + 1
        if userFound == 0:
            print 'User is not logged in the existing database, calling their getProfile API...'
            try:    
                data = {"profile_username":profile_username,"sender":cherrypy.session['username']}
                IP = str(self.getOnlineUserDetails(profile_username,'ip'))
                port = str(self.getOnlineUserDetails(profile_username,'port'))
                data = json.dumps(data)
                url = 'http://' + IP + ':' + port + '/getProfile?'
                req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
                print profile_username + "'s getProfile is called"
                response = urllib2.urlopen(req)
                response = response.read()
                input_data = json.loads(response)
                print input_data
                requestedFullname = input_data['fullname']
                requestedPosition = input_data['position']
                requestedDescription = input_data['description']
                requestedLocation = input_data['location']
                requestedPicture = input_data['picture']
                loggedUser = self.logUserProfile(profile_username, requestedFullname, requestedPosition, requestedDescription, requestedLocation, requestedPicture)
                if loggedUser == 1:
                    Page += '<b>user found!</b><br><br>'
                    picture = self.getProfileDetails(profile_username,'picture')
                    Page += '<img src="' + picture + '" width = 200/>'
                    Page += "<center><br><b>" + profile_username + "</b>'s profile<br>"
                    Page += "<br><sub>Fullname:</sub><br>" + self.getProfileDetails(profile_username,'fullname') 
                    Page += "<br><sub>Location:</sub><br>" + self.getProfileDetails(profile_username,'location')
                    Page += "<br><sub>Description:</sub><br>" + self.getProfileDetails(profile_username,'description')
                    Page += "<br><sub>Position:</sub><br>" + self.getProfileDetails(profile_username,'position')
            except:
                Page += 'Sorry user does not exist in database and their profile could not be retrieved :('
        return Page        
        
            
    #Allows user to receiveFile from another user by receiving json data which is then processed
    #and logged into the receivedMessages according to their columns.
    #Returns an error code depending on the success of the file receiving process
    #Prints out a message to the console which tells the user that a file has been received along
    #with the sender's username        
    @cherrypy.expose
    @cherrypy.tools.json_in()    
    def receiveFile(self,encryption=None,hashing=None,hash=None,decryptionKey=None):

        if encryption != None or decryptionKey != None:
            return '9: Encryption Standard Not Supported'
        elif hashing != None:
            return '10: Hashing Standard Not Supported'
        elif hash != None:
            return '7: Hash does not match'
        else:
            input_data = cherrypy.request.json
            sender = input_data['sender']
            destination = input_data['destination']
            file = input_data['file']
            filename = input_data['filename']
            content_type = str(input_data['content_type'])
            content_type = content_type.split('/')
            content_type = content_type[1]
            stamp = float(input_data['stamp'])
            stamp = time.strftime("%Y-%m-%d %H:%M", time.localtime(stamp)) 
            message = None
            messageDetails = [sender, destination, message, stamp, file, filename, content_type]
            self.logReceivedMessage(messageDetails)
            print str(sender) + ' sent ' + str(filename)
            return '0: <Action was succesful>'

    #Allows user to sendFile to another user by sending json data
    #Sent files are not being logged into the sentMessages table
    #Reads the error code received from the other user and prints out a message to the console which 
    #tells the user that a file has been sent along with the receiver's username    
    @cherrypy.expose  
    def sendFile(self, myFile, destination):
        file_read = myFile.file.read()
        file_64_encode = base64.encodestring(file_read)
     
        sender = cherrypy.session['username']
        file = file_64_encode
        filename = myFile.filename
        content_type = myFile.content_type
        stamp = time.time()
        destinationIP = str(self.getOnlineUserDetails(destination,'ip'))
        destinationPort = str(self.getOnlineUserDetails(destination,'port')) 
        sendingFile = {"sender":str(sender),"destination":str(destination),"file":str(file),"filename":str(filename),"content_type":str(content_type),"stamp":str(stamp)}
        data = json.dumps(sendingFile)
        url = 'http://'+destinationIP+':'+destinationPort+'/receiveFile'
        req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req).read()
        
        if str(response)[0] == '0':
            print 'File has been sent to ' + str(destination)
            return '<center><img src="https://s-media-cache-ak0.pinimg.com/originals/96/cb/f1/96cbf1cb56de8d012513832d664d0902.gif" width = 400;/><br><br><body style="background-color:#E7D3CE">File has been sent to ' + str(destination) + " at " + time.strftime("%Y-%m-%d %H:%M", time.localtime(stamp)) + "<br>Return to <a href='interact'>/interact</a>"
    
        
        
def runMainApp():

    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/")
    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                            'tools.gzip.on' : True,
                            'tools.gzip.mime_types' : ['text/*'],
                           })

    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"                       
    
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()
 
#Run the function to start everything
runMainApp()