# My Project's README
COMPSYS302 - Project B
Ira Syamira Sukimin (isuk218)

1. Boot Linux
2. Clone/download repository to preferred directory
3. Open terminal and change directory to the directory of the cloned/downloaded repository
4. Type in "python MainApp.py" and press Enter
5. A browser tab should open automatically and user will be asked to enter their credentials

Application tested with: stoo718 and smoh944
Have tried running the application in another user's account which is not from the ECE dept